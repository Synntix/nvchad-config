local M = {}

M.general = {
  n = {
    ["<C-h>"] = { "<cmd> TmuxNavigateLeft<CR>", "window left" },
    ["<C-j>"] = { "<cmd> TmuxNavigateDown<CR>", "window down" },
    ["<C-k>"] = { "<cmd> TmuxNavigateUp<CR>", "window up" },
    ["<C-l>"] = { "<cmd> TmuxNavigateRight<CR>", "window right" },
  }
}

-- add this table only when you want to disable default keys
M.aerial = {
  n = {
      ["<leader>a"] = { "<cmd>AerialToggle!<CR>", "toggle outline" },
  }
}

M.dap = {
  n = {
    ["<Leader>d"] = {'<Cmd>lua require"dapui".toggle()<CR>', "Toggle DAP UI"},
    ["<A-b>"] = {'<Cmd>lua require"dap".toggle_breakpoint()<CR>', "Toggle breakpoint"},
    ["<A-c>"] = {'<Cmd>:lua require"dap".continue()<CR>', "DAP continue"},
    ["<A-s>"] = {'<Cmd>lua require"dap".step_over()<CR>', "DAP step over"},
    ["<A-S>"] = {'<Cmd>lua require"dap".step_into()<CR>', "DAP step into"},
  }
}

M.crates = {
  n = {
    ["<leader>rcu"] = {
      function()
        require("crates").upgrade_all_crates()
      end,
      "update all crates"
    }
  }
}

-- Your custom mappings
M.perso = {
  n = {
    ["<A-f>"] = {":Telescope fd <CR>", "Telescope Files"},
    ["<A-R>"] = {"<Cmd>lua require('nvterm.terminal').send('cargo run' .. '\\r', 'float')<CR>", "Run cargo run in terminal"},
  }
}

return M
